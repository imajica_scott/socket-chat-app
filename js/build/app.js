// the login prompt/top top level just needs to know...are you authenticated with the socket or not
// possible states are:
// not connected > show login prompt clean
// unauthorised > show login prompt with error
// connecting > show connecting status
// authorised > show main window with chat and sidebar
"use strict";

var Main = React.createClass({
	displayName: "Main",

	// this is the top level component for the entire application
	getInitialState: function getInitialState() {
		return {
			"socketState": "not connected",
			"name": ""
		};
	},
	connect: function connect(nameValue) {
		// this will cause us to mount <ChatContainer /> which triggers a socket connection attempt
		this.setState({ "socketState": "connecting", "name": nameValue });
	},
	updateSocketState: function updateSocketState(strState) {
		this.setState({ "socketState": strState });
	},
	render: function render() {
		// when we render out ChatContainer...possibly pass a prop for a namespace/private message etc
		// to determine how to connect with the websocket
		switch (this.state.socketState) {
			case "not connected":
			case "unauthorised":
				var element = React.createElement(JoinPrompt, { connect: this.connect, socketState: this.state.socketState });
				break;
			case "connecting":
				var element = React.createElement(ChatContainer, {
					updateState: this.updateSocketState,
					connectionState: this.state.socketState,
					name: this.state.name });
				break;
			case "authorised":
				var element = React.createElement(ChatContainer, {
					updateState: this.updateSocketState,
					connectionState: this.state.socketState,
					name: this.state.name });
				break;
		}
		return React.createElement(
			"div",
			{ className: "container" },
			element
		);
	}
});

var JoinPrompt = React.createClass({
	displayName: "JoinPrompt",

	render: function render() {
		return React.createElement(
			"div",
			{ className: "promptContainer" },
			React.createElement(
				"div",
				{ className: "connectPrompt" },
				React.createElement(
					"div",
					{ className: "promptText" },
					"Welcome!"
				),
				React.createElement(JoinForm, { connect: this.props.connect })
			)
		);
	}
});

var JoinForm = React.createClass({
	displayName: "JoinForm",

	getInitialState: function getInitialState() {
		return {
			"inputValue": ""
		};
	},
	componentDidMount: function componentDidMount() {
		React.findDOMNode(this.refs.nameInput).focus();
	},
	update: function update(event) {
		this.setState({
			"inputValue": event.target.value
		});
	},
	submit: function submit(event) {
		event.preventDefault();
		if (this.state.inputValue.length > 2) {
			this.props.connect(this.state.inputValue);
			this.setState({
				"inputValue": ""
			});
		} else {
			return false;
		}
	},
	render: function render() {
		return React.createElement(
			"form",
			{ action: "", id: "nameInputForm", onSubmit: this.submit },
			React.createElement("input", {
				type: "text",
				id: "nameInput",
				placeholder: "Who are you?",
				value: this.state.inputValue,
				onChange: this.update,
				ref: "nameInput" }),
			React.createElement(
				"button",
				{
					id: "join",
					className: this.state.inputValue !== undefined && this.state.inputValue > 2 ? "" : "inactive" },
				"Connect"
			)
		);
	}
});

var ChatContainer = React.createClass({
	displayName: "ChatContainer",

	getInitialState: function getInitialState() {
		return {
			"messages": [],
			"connectedUsers": [],
			"typingUsers": [],
			"selfID": ""
		};
	},
	componentDidMount: function componentDidMount() {
		// create our socket.io connection here
		// all listeners on the socket connection must listen here (possibly not true, continuing for code structure anyway)
		if (this.props.connectionState === "connecting") {
			this.socket = io(window.location.href !== "http://localhost:3000/" ? window.location.href === "http://socket-chat-app.herokuapp.com/" ? "" : 'http://192.168.0.23/' : "", { "authValue": "fooAuth" });
			// setup our listeners for different events
			this.socket.emit('authoriseMe', this.props.name);
			this.socket.on('authorised', (function () {
				// preventing us calling setState from within componentDidMount (a no-op)
				// having these two functions (setUsers and setMessages) as methods on the component probably isn't necessary...
				this.socket.on('user state', this.setUsers);
				this.socket.on('existing messages', this.setMessages);
				this.socket.on('chat message', this.newMessage);
				this.socket.on('user disconnect', this.userDisconnected);
				this.socket.on('user typing', this.addTyping);
				this.socket.on('user stop typing', this.removeTyping);
				this.props.updateState('authorised');
			}).bind(this));
			this.socket.on('not authorised', (function () {
				// this line will remove all the existing listeners on our socket
				// because we setup the socket, then we wait for authentication (that wait is a listener)
				// if we are not authenticated, we are still listening for authentication on that socket
				// this clears out that listener, so that when we do retry, we listen again for authentication
				this.socket.removeAllListeners();
				// this.socket = null; // probably not necessary - here just incase it may fix future issue
				this.props.updateState('unauthorised');
			}).bind(this));
		}
	},
	addTyping: function addTyping(string) {
		if (string !== "") {
			this.setState(function (previousState, currentProps) {
				return previousState.typingUsers.push(string);
			});
		} else if (this.state.typingUsers.indexOf(this.props.name) === -1) {
			this.setState(function (previousState, currentProps) {
				return previousState.typingUsers.push({ "name": this.props.name, "selfID": this.state.selfID });
			});
		}
	},
	removeTyping: function removeTyping(string) {
		if (string !== "") {
			this.setState(function (previousState, currentProps) {
				return previousState.typingUsers.splice(this.state.typingUsers.indexOf(string), 1);
			});
		} else if (this.state.typingUsers.indexOf(this.props.name) !== -1) {
			this.setState(function (previousState, currentProps) {
				return previousState.typingUsers.splice(this.state.typingUsers.indexOf(this.props.name), 1);
			});
		}
	},
	setMessages: function setMessages(messagesArray) {
		var existingState = this.state;
		existingState.messages = messagesArray;
		this.setState(existingState);
	},
	newMessage: function newMessage(message, user) {
		if (typeof message === "string") {
			var messageObj = {
				"message": message,
				"name": this.props.name
			};
		} else {
			var messageObj = message;
		}
		this.socket.emit('chat message', message);
		this.setState(function (previousState, currentProps) {
			return previousState.messages.push(messageObj);
		});
	},
	setUsers: function setUsers(users) {
		// console.log(users)
		this.setState({
			"connectedUsers": users.users,
			"typingUsers": users.typing,
			"selfID": users.users.filter((function (item, index) {
				return item.name === this.props.name;
			}).bind(this))[0].id
		});
	},
	userDisconnected: function userDisconnected(userObj) {
		this.setState(function (previousState, currentProps) {
			return previousState.connectedUsers.splice(this.state.connectedUsers.indexOf(userObj), 1);
		});
	},
	render: function render() {
		var chatFns = {
			newMessage: this.newMessage,
			addTyping: this.addTyping,
			removeTyping: this.removeTyping
		};
		return React.createElement(
			"div",
			null,
			this.props.connectionState === "connecting" ? React.createElement(ChatLoading, null) : React.createElement(ChatWindow, { state: this.state, connectionState: this.props.connectionState, chatFns: chatFns })
		);
	}
});

var ChatLoading = React.createClass({
	displayName: "ChatLoading",

	render: function render() {
		return React.createElement(
			"div",
			null,
			"Loading... (center me please :D or maybe fa-cog/loading gif?)"
		);
	}
});
var ChatWindow = React.createClass({
	displayName: "ChatWindow",

	render: function render() {
		return React.createElement(
			"div",
			null,
			React.createElement(SideBar, { users: this.props.state.connectedUsers, state: this.props.connectionState }),
			React.createElement(ChatBox, {
				messages: this.props.state.messages,
				typing: this.props.state.typingUsers,
				chatFns: this.props.chatFns })
		);
	}
});

var SideBar = React.createClass({
	displayName: "SideBar",

	render: function render() {
		return React.createElement(
			"div",
			{ className: "statusContainer" },
			React.createElement(
				"div",
				{ className: "statusInnerContainer" },
				React.createElement(
					"div",
					{ id: "headerBox", className: "headerBox" },
					React.createElement(
						"div",
						{ className: "logoContainer" },
						React.createElement(
							"span",
							{ className: "mainHeader" },
							"Socket Chat App"
						),
						React.createElement(
							"div",
							{ className: this.props.state === "authorised" ? "connectState Connected" : "connectState" },
							this.props.state === "authorised" ? "Connected" : "Connecting..."
						),
						React.createElement("div", { className: "setMeFree" })
					)
				),
				React.createElement(UserBox, { users: this.props.users })
			)
		);
	}
});

var UserBox = React.createClass({
	displayName: "UserBox",

	render: function render() {
		return React.createElement(
			"div",
			{ id: "userBox", className: "userBox" },
			this.props.users.map(function (item, index) {
				return React.createElement(User, { key: index, name: item.name, id: item.id });
			})
		);
	}
});

var ChatBox = React.createClass({
	displayName: "ChatBox",

	getInitialState: function getInitialState() {
		return {
			"inputValue": ""
		};
	},
	submitMessage: function submitMessage(event) {
		event.preventDefault();
		this.props.chatFns.removeTyping("");
		this.props.chatFns.newMessage(this.state.inputValue);
		this.setState({ "inputValue": "" });
	},
	componentDidMount: function componentDidMount() {
		React.findDOMNode(this.refs.chatInput).focus();
	},
	updateValue: function updateValue(event) {
		if (event.target.value === "") {
			this.props.chatFns.removeTyping("");
		} else {
			this.props.chatFns.addTyping("");
		}
		this.setState({ "inputValue": event.target.value });
	},
	typingState: function typingState(typingUsers) {
		if (typingUsers.length === 1) {
			if (typingUsers[0] === "You") {
				return typingUsers[0] + " are currently typing"; // necessary?
			} else {
					return typingUsers[0] + " is currently typing";
				}
		} else if (typingUsers.length === 2) {
			return typingUsers[0] + " and " + typingUsers[1] + " are typing";
		} else if (typingUsers.length === 3) {
			return typingUsers[0] + ", " + typingUsers[1] + " and " + typingUsers[2] + " are typing";
		} else if (typingUsers.length >= 4) {
			return typingUsers[0] + ", " + typingUsers[1] + " and " + (typingUsers.length - 2) + " others are typing";
		} else {
			return "";
		}
	},
	render: function render() {
		return React.createElement(
			"div",
			{ className: "chatContainer socketOpen" },
			React.createElement(
				"ul",
				{ id: "messages" },
				this.props.messages.map(function (item, index) {
					return React.createElement(
						"li",
						{ key: index },
						item.name,
						": ",
						item.message
					);
				})
			),
			React.createElement(
				"div",
				{ id: "typingStatus", className: "typingStatus socketOpen" },
				this.typingState(this.props.typing)
			),
			React.createElement(
				"form",
				{ action: "", id: "messageForm", className: "socketOpen", onSubmit: this.submitMessage },
				React.createElement("input", { id: "m", autoComplete: "off", ref: "chatInput", value: this.state.inputValue, onChange: this.updateValue }),
				React.createElement(
					"button",
					null,
					"Send"
				)
			)
		);
	}
});

var User = React.createClass({
	displayName: "User",

	render: function render() {
		return React.createElement(
			"div",
			{ className: "user" },
			this.props.name,
			" - Online"
		);
	}
});
// <div>
// 	This is my chatbox!
// 	{this.props.messages.toString()}
// </div>

React.render(React.createElement(Main, null), document.body);