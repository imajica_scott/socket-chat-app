$(".socketOpen").hide()
$("#nameInput").focus()
$("#nameInput").on("keyup", function () {
	if ($(this).val().length > 2) {
		$("#join").removeClass("inactive")
		$(this).removeClass("error")
	} else {
		$("#join").addClass("inactive")
	}
})

var pageState = { // move this into react state
	messages : [], // array of messages (with 'name' and 'msg' fields)
	currentlyTyping : [], // array of currently typing users
	currentlyTypingList : function () {
		return this.currentlyTyping.buildUserList(); // converts the list of users into a human-readable list
	},
	serverState : 'Not Connected' // stores the current connection state to the server
};
// $("#serverState").html(pageState.serverState);
function submitNameInput(event) {
	if ($("#nameInput").val().length > 3) {
	    var socket = io((window.location.href !== "http://localhost:3000/" ? (window.location.href === "http://socket-chat-app.herokuapp.com/" ? "" : 'http://192.168.0.46/') : ""));
	    window.userObj = {
	    	name : $("#nameInput").val(),
	    	msg : ""
	    }
	    $(".promptContainer").hide()
	    createSocket(socket)
	} else {
		$("#nameInput").addClass("error")
	}
}
$("#join").on("click", function () {
	submitNameInput()
})


function createSocket (socket) {
	var typing = false;
	$(".socketOpen").show()
	$("#m").focus()
	$("#m").on("keyup keydown change click keyup input paste propertychange", function () {
		if (typing && $(this).val().length != 0) {
			typing = true; // basically do nothing
		} else {
			if (typing) {
				if ($(this).val().length === 0) {
					typing = false; // if typing is true, but the input value length is 0
    				socket.emit('userStopTyping', userObj)
				} else {
					typing = true; // we should never get here...the first step should pick this up
				}
			} else if (!typing && $(this).val().length != 0) {
				typing = true;
				socket.emit('userTyping', userObj)
			}
		}
	})
    $("form#messageForm").submit( function (event) {
    	event.preventDefault();
    	userObj.msg = $("#m").val();
    	// $('#messages').append($('<li>').text(userObj.name + " : " + userObj.msg));
    	socket.emit('chat message', userObj);
    	$('#m').val('');
    	return false;
    });

 	socket.on('connect', function (obj) {
 		socket.emit('connectConfirm', userObj)
 		pageState.serverState = 'Connected';
 		// $("#serverState").html(pageState.serverState);
 	})
 	socket.on('disconnect', function (obj) {
 		pageState.serverState = 'Disconnected';
 		// $("#serverState").html(pageState.serverState);
 	})
 	socket.on('chat message', function (obj) {
 		pageState.messages.push(obj);
 		// $("#messages").html(Mustache.render(messageTemplate, pageState));
 		$('#messages').append($('<li>').html(obj.name + ' : ' + obj.msg));
 	})
 	socket.on('userconnect', function (obj) {
 		$('#messages').append($('<li style="color:green;">').text(obj.name + " just connected"));
 	})
 	socket.on('userdisconnect', function (obj) {
 		$('#messages').append($('<li style="color:red;">').text(obj.name + " just left"));
 	})
 	socket.on('usersTyping', updateTypingState);
 	socket.on('userState', function (obj) {
 		updateTypingState(obj.typing);
 		updateConnectedUsers(obj.users);
 	})
}

function updateConnectedUsers(users) {
	console.log(users);
}
function updateTypingState (usersTyping) {
	if (usersTyping.length != 0) {
		pageState.currentlyTyping = usersTyping;
		if (usersTyping.indexOf(userObj.name) != -1) {
			var yourIndex = pageState.currentlyTyping.indexOf(userObj.name);
			pageState.currentlyTyping[yourIndex] = 'You';
			pageState.currentlyTyping.swap(yourIndex, 0);
		}
	} else {
		pageState.currentlyTyping = [];
	}
	$("#typingStatus").html(pageState.currentlyTypingList());
}
Array.prototype.buildUserList = function () {
	if (this.length === 1) {
		if (this[0] === "You") {
			return this[0] + " are currently typing"; // necessary?
		} else {
			return this[0] + " is currently typing";
		}
	} else if (this.length === 2) {
		return this[0] + " and " + this[1] + " are typing";
	} else if (this.length === 3) {
		return this[0] + ", " + this[1] + " and " + this[2] + " are typing";
	} else if (this.length >= 4) {
		return this[0] + ", " + this[1] + " and " + (this.length - 2) + " others are typing";
	} else {
		return "";
	}
}
Array.prototype.swap = function (x,y) {
  var b = this[x];
  this[x] = this[y];
  this[y] = b;
  return this;
}
