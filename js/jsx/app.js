// the login prompt/top top level just needs to know...are you authenticated with the socket or not
// possible states are:
// not connected > show login prompt clean
// unauthorised > show login prompt with error
// connecting > show connecting status
// authorised > show main window with chat and sidebar
var Main = React.createClass({
	// this is the top level component for the entire application
	getInitialState : function () {
		return (
				{
					"socketState" : "not connected",
					"name" : ""
				}
			);
	},
	connect : function (nameValue) {
		// this will cause us to mount <ChatContainer /> which triggers a socket connection attempt
		this.setState({"socketState" : "connecting", "name" : nameValue});
	},
	updateSocketState : function (strState) {
		this.setState({"socketState" : strState});
	},
	render : function () {
		// when we render out ChatContainer...possibly pass a prop for a namespace/private message etc
		// to determine how to connect with the websocket
		switch (this.state.socketState) {
						case "not connected":
						case "unauthorised":
							var element = <JoinPrompt connect={this.connect} socketState={this.state.socketState} />
							break;
						case "connecting":
							var element = <ChatContainer 
												updateState={this.updateSocketState} 
												connectionState={this.state.socketState} 
												name={this.state.name} />
							break;
						case "authorised":
							var element = <ChatContainer 
												updateState={this.updateSocketState} 
												connectionState={this.state.socketState} 
												name={this.state.name} /> 
							break;
					}
		return (
				<div className="container">
					{element}
				</div>
			)
	}
})

var JoinPrompt = React.createClass({
	render : function () {
		return (
				<div className="promptContainer">
					<div className="connectPrompt">
						<div className="promptText">Welcome!</div>
						<JoinForm connect={this.props.connect} />
					</div>
				</div>	
			)
	}
})

var JoinForm = React.createClass({
	getInitialState : function () {
		return (
				{
					"inputValue" : ""
				}
			)
	},
	componentDidMount : function () {
		React.findDOMNode(this.refs.nameInput).focus();
	},
	update : function (event) {
		this.setState({
			"inputValue" : event.target.value
		});
	},
	submit : function (event) {
		event.preventDefault();
		if (this.state.inputValue.length > 2) {
			this.props.connect(this.state.inputValue);
			this.setState({
				"inputValue" : ""
			})
		} else {
			return false;
		}
	},
	render : function () {
		return (
				<form action="" id="nameInputForm" onSubmit={this.submit}>
					<input 
						type="text" 
						id="nameInput" 
						placeholder="Who are you?" 
						value={this.state.inputValue} 
						onChange={this.update}
						ref="nameInput" />
					<button 
						id="join" 
						className={(this.state.inputValue !== undefined && this.state.inputValue > 2) ? "" : "inactive"}>
							Connect
					</button>
				</form>
			)
	}
})

var ChatContainer = React.createClass({
	getInitialState : function () {
		return {
			"messages" : [],
			"connectedUsers" : [],
			"typingUsers" : [],
			"selfID" : ""
		};
	},
	componentDidMount : function () {
		// create our socket.io connection here
		// all listeners on the socket connection must listen here (possibly not true, continuing for code structure anyway)
		if (this.props.connectionState === "connecting") {
			this.socket = io((window.location.href !== "http://localhost:3000/" ?
							(window.location.href === "http://socket-chat-app.herokuapp.com/" ? "" : 'http://192.168.0.23/') : ""), 
							{"authValue" : "fooAuth"});
			// setup our listeners for different events
			this.socket.emit('authoriseMe', this.props.name);
			this.socket.on('authorised', function () {
				// preventing us calling setState from within componentDidMount (a no-op)
				// having these two functions (setUsers and setMessages) as methods on the component probably isn't necessary...
				this.socket.on('user state', this.setUsers); 
				this.socket.on('existing messages', this.setMessages);
				this.socket.on('chat message', this.newMessage);
				this.socket.on('user disconnect', this.userDisconnected);
				this.socket.on('user typing', this.addTyping)
				this.socket.on('user stop typing', this.removeTyping)
				this.props.updateState('authorised');
			}.bind(this))
			this.socket.on('not authorised', function () {
				// this line will remove all the existing listeners on our socket
				// because we setup the socket, then we wait for authentication (that wait is a listener)
				// if we are not authenticated, we are still listening for authentication on that socket
				// this clears out that listener, so that when we do retry, we listen again for authentication
				this.socket.removeAllListeners();
				// this.socket = null; // probably not necessary - here just incase it may fix future issue
				this.props.updateState('unauthorised');
			}.bind(this))
		}
	},
	addTyping : function (string) {
		if (string !== "") {
			this.setState(function (previousState, currentProps) {
				return previousState.typingUsers.push(string);
			});
		} else if (this.state.typingUsers.indexOf(this.props.name) === -1) {
			this.setState(function (previousState, currentProps) {
				return previousState.typingUsers.push({"name" : this.props.name, "selfID" : this.state.selfID});
			});
		} 
	},
	removeTyping : function (string) {
		if (string !== "") {
			this.setState(function (previousState, currentProps) {
				return previousState.typingUsers.splice(this.state.typingUsers.indexOf(string), 1);
			})
		} else if (this.state.typingUsers.indexOf(this.props.name) !== -1) {
			this.setState(function (previousState, currentProps) {
				return previousState.typingUsers.splice(this.state.typingUsers.indexOf(this.props.name), 1)
			})
		}
	},
	setMessages : function (messagesArray) {
		var existingState = this.state;
		existingState.messages = messagesArray;
		this.setState(existingState);
	},
	newMessage : function (message, user) {
		if (typeof message === "string") {
			var messageObj = {
				"message" : message,
				"name" : this.props.name
			}
		} else {
			var messageObj = message;
		}
		this.socket.emit('chat message', message)
		this.setState(function (previousState, currentProps) {
			return previousState.messages.push(messageObj);
		});
	},
	setUsers : function (users) {
		// console.log(users)
		this.setState({
			"connectedUsers" : users.users, 
			"typingUsers" : users.typing, 
			"selfID" : users.users.filter(function (item, index) {
				return item.name === this.props.name;
			}.bind(this))[0].id
		});
	},
	userDisconnected : function (userObj) {
		this.setState(function (previousState, currentProps) {
			return previousState.connectedUsers.splice(this.state.connectedUsers.indexOf(userObj), 1)
		})
	},
	render : function () {
		var chatFns = {
			newMessage : this.newMessage,
			addTyping : this.addTyping,
			removeTyping : this.removeTyping
		};
		return (
				<div>
					{(this.props.connectionState === "connecting") ? <ChatLoading /> : 
							<ChatWindow state={this.state} connectionState={this.props.connectionState} chatFns={chatFns} />}
				</div>
			)
	}
})

var ChatLoading = React.createClass({
	render : function () {
		return (
				<div>
					Loading... (center me please :D or maybe fa-cog/loading gif?)
				</div>
			)
	}
})
var ChatWindow = React.createClass({
	render : function () {
		return (
				<div>
					<SideBar users={this.props.state.connectedUsers} state={this.props.connectionState} />
					<ChatBox 
						messages={this.props.state.messages} 
						typing={this.props.state.typingUsers} 
						chatFns={this.props.chatFns} />
				</div>
			)
	}
})

var SideBar = React.createClass({
	render : function () {
		return (
				<div className="statusContainer">
					<div className="statusInnerContainer">
						<div id="headerBox" className="headerBox">
							<div className="logoContainer">
								<span className="mainHeader">Socket Chat App</span>
								<div className={(this.props.state === "authorised") ? "connectState Connected" : "connectState"}>
									{(this.props.state === "authorised") ? "Connected" : "Connecting..."}
								</div>
								<div className="setMeFree"></div>
							</div>
						</div>
						<UserBox users={this.props.users} />
					</div>
				</div>
			)
	}
});

var UserBox = React.createClass({
	render : function () {
		return (
				<div id="userBox" className="userBox">
					{
						this.props.users.map(function (item, index) {
							return <User key={index} name={item.name} id={item.id} />
						})
					}
				</div>
			)

	}
})

var ChatBox = React.createClass({
	getInitialState : function () {
		return {
			"inputValue" : ""
		};
	},
	submitMessage : function (event) {
		event.preventDefault();
		this.props.chatFns.removeTyping("");
		this.props.chatFns.newMessage(this.state.inputValue);
		this.setState({"inputValue":""});
	},
	componentDidMount : function () {
		React.findDOMNode(this.refs.chatInput).focus();
	},
	updateValue : function (event) {
		if (event.target.value === "") {
			this.props.chatFns.removeTyping("");
		} else {
			this.props.chatFns.addTyping("");
		}
		this.setState({"inputValue" : event.target.value});
	},
	typingState : function (typingUsers) {
		if (typingUsers.length === 1) {
			if (typingUsers[0] === "You") {
				return typingUsers[0] + " are currently typing"; // necessary?
			} else {
				return typingUsers[0] + " is currently typing";
			}
		} else if (typingUsers.length === 2) {
			return typingUsers[0] + " and " + typingUsers[1] + " are typing";
		} else if (typingUsers.length === 3) {
			return typingUsers[0] + ", " + typingUsers[1] + " and " + typingUsers[2] + " are typing";
		} else if (typingUsers.length >= 4) {
			return typingUsers[0] + ", " + typingUsers[1] + " and " + (typingUsers.length - 2) + " others are typing";
		} else {
			return "";
		}
	},
	render : function () {
		return (
		  	<div className="chatContainer socketOpen">
			    <ul id="messages">
			    	{
			    		this.props.messages.map(function (item, index) {
			    			return <li key={index}>{item.name}: {item.message}</li>;
			    		})
			    	}
			    </ul>
			    <div id="typingStatus" className="typingStatus socketOpen">{this.typingState(this.props.typing)}</div>
			    <form action="" id="messageForm" className="socketOpen" onSubmit={this.submitMessage}>
			      <input id="m" autoComplete="off" ref="chatInput" value={this.state.inputValue} onChange={this.updateValue} />
			      <button>Send</button>
			    </form>
		  	</div>
			)
	}
});

var User = React.createClass({
	render : function () {
		return (
				<div className="user">
					{this.props.name} - Online
				</div>
			)
	}
})
			  	// <div>
				// 	This is my chatbox!
				// 	{this.props.messages.toString()}
				// </div>

React.render(<Main/>, document.body)