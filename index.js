var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = (process.env.NODE_ENV == 'production') ? process.env.PORT : 3000; // browser-sync doesn't realise 3000 is in use...
var Filter = require('bad-words')
var emojione = require('emojione');
var customFilter = new Filter({ placeHolder : emojione.shortnameToImage(':poop:')});

app.get('/', function (req, res) {
	res.sendFile(__dirname + '/index.html');
});

// feels a little insecure...keep these folders empty and we are ok i think
app.get(/(css)|(js)/, function (req, res) {
	res.sendFile(__dirname + req.url);
});

var connectedUsers = [];
var usersTyping = [];
var socketAuthorised = false;
var serverMessages = [
	{name : "scott", message : "bar1"},
	{name : "john", message : "bar2"},
	{name : "mark", message : "bar3"},
	{name : "david", message : "bar4"}
];
var allowedNames = ["scott", "john", "mark", "foo", "bar"];


io.on('connection', function (socket) {
	socket.on('authoriseMe', function (name) {
		if (allowedNames.indexOf(name) !== -1) {
			socketAuthorised = true;
			// prevent duplicate users
			connectedUsers.push({id: socket.id, name : name});
			allowedNames.splice(allowedNames.indexOf(name), 1);
			socket.emit('authorised');
			console.log("emitted authorised event")
			socket.emit('user state', {typing : usersTyping, users : connectedUsers});
			socket.emit('existing messages', serverMessages);
		} else {
			console.log(allowedNames)
			console.log(connectedUsers)
			socketAuthorised = false;
			socket.emit('not authorised')
		}
	})
	setTimeout(function () {
		io.emit('chat message', {name : "foo", message : "bar"});
	}, 3000);
	setTimeout(function () {
		io.emit('chat message', {name : "foo", message : "man"});
	}, 5000);
	// when a user first connects we want to...
	// put them in a unique room according to their id when they connected - done automatically
	// 		in future, this could attempt to grab an IP/MAC address and save their conversation if they crash/disconnect
	// make sure each user can send and receive in those two sperate rooms at the same time
	// build an admin page to handle multiple chats from different rooms at once
	socket.on('disconnect', function () {
		// loop through connected users
		for (i = 0; i < connectedUsers.length; i++) {
			if (connectedUsers[i].id === socket.id) {
				console.log("disconnecting user: %s", connectedUsers[i].name)
				// add their name back in to the allowed users list
				allowedNames.push(connectedUsers[i].name);
				// take their name out of the currently typing list
				usersTyping.splice(usersTyping.indexOf(connectedUsers[i]), 1)
				// tell everyone that they have disconnected
				io.emit('userdisconnect', connectedUsers[i]);
				// update everyone with the new typing list
				io.emit('usersTyping', usersTyping);
				// finally, remove them from our connected users array
				connectedUsers.splice(i, 1);
			}
		}
	})
	// socket.on('chat message', function(obj){
	// 	obj.msg = customFilter.clean(obj.msg);
	// 	// io.to(socket.id).emit('chat message', obj);
	// 	io.emit('chat message', obj);
	// 	console.log('message sent to %s', socket.id);
	// 	// console.log(io.nsps["/"].sockets);
	// })
	socket.on('connectConfirm', function(obj){
		// io.emit('new user', obj)
		// socket.broadcast.emit('userconnect', obj);
		var userObj = {
			id : socket.id,
			name : obj.name
		}
		connectedUsers.push(userObj)
		io.emit('usersTyping', usersTyping);
	})

	socket.on('userTyping', function (obj) {
		if (socketAuthorised) {
			usersTyping.push(obj)
			io.emit('usersTyping', usersTyping)
		}
	})

	socket.on('userStopTyping', function (obj) {
		if (socketAuthorised) {
			usersTyping.splice(usersTyping.indexOf(obj), 1)
			io.emit('usersTyping', usersTyping)
		}
	})
})

http.listen(port, function (){
	console.log('listening on *:' + port);
})