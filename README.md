# Socket.io Chat App #

Testing the functionality of Socket.io, Nodejs and ExpressJS with a relatively straightforward ReactJS frontend

## Done ##
* Broadcast a message to connected users when someone connects or disconnects
* Add support for nicknames
* Dont send the same message to the user that sent it himself. Instead, append the message directly as soon as he presses enter.
* Add {user} is typing functionality
* Deploy to heroku
* Implement better user status handling - using mustache - now abandoned for react
* Fix behaviour for "%s is typing..."
* Add connection status indicator - rudimentary styling
* Setup React and Babel with Gulp
* Styling setup for current users
* messages

## TODO ##

* fix is currently typing
* Implement namespaces or channels
* Setup React code and styling for:
* connection state (disconnection + reconnection)

## LONG-TERM TODO ##

* Add private messaging
* Store messages up to predefined date (involves attaching rethinkdb/mongo)
* -Idea- assign a score server side for commonality/closeness per user and send to client side to define ordering in UserBox (similar to FB messenger - would also involve attaching rethink/mongo)
* Get the project working with browser-sync (it runs on socket.io and conflict occurs) (maybe setup seperate client and just use heroku as server?)
* Animate the text from the input upwards when user types a message

## Info ##

### Project Setup Steps ###

* clone repo
* `npm install`
* 'node index.js' (for local server, my heroku setup below)
* install heroku toolbelt
* `heroku login`
* `heroku local` to run locally
* Run `heroku git:remote -a socket-chat-app`
* Commit and `git push heroku master` to run a deployment