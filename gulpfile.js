var gulp = require('gulp');
// var browserSync = require('browser-sync');
// var reload = browserSync.reload;
var less = require('gulp-less');
var uglify = require('gulp-uglify');
var notify = require('gulp-notify');
var plumber = require('gulp-plumber');
var babel = require('gulp-babel');
// var browserify = require('browserify');
// var source = require('vinyl-source-stream');

// gulp.task('browser-sync', function () {
// 	browserSync({
// 		server: {
// 			baseDir: "./"
// 		}
// 		// browser: "chrome",
// 		// xip: true
// 		// proxy: "test.domain.com:8888/"
// 	});
// });

// Compiles our less
gulp.task('less', function () {
	return gulp.src('./less/styles.less')
	.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
	.pipe(less())
	.pipe(plumber.stop())
	.pipe(gulp.dest('./css'))
	// .pipe(reload({stream:true}))
});

// Babel/JSX Transform our jsx templates
gulp.task('babel', function () {
	return gulp.src("./js/jsx/*.js")
		.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
		.pipe(babel())
		.pipe(plumber.stop())
		.pipe(gulp.dest('./js/build/'));
})


gulp.task('watch', function () {
	// gulp.watch("**/*.html").on('change', reload);
	// gulp.watch("**/*.cfm").on('change', reload);
	// gulp.watch('./js/**/*.js').on('change', reload);
	gulp.watch('./less/**/*.less', ['less']);
	gulp.watch('./js/jsx/*.js', ['babel']);
	// gulp.watch('./css/**/*.css', ['streamCSS']);
});

// gulp.task('default', ['watch', 'less', 'browser-sync']);
gulp.task('default', ['watch', 'less', 'babel']);